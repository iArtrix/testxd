﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsXD
{
    public partial class Form3 : Form
    {
        private Form1 form;
        public Form3(Form1 form)
        {
            this.form = form;
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.form.SendData = textBox1.Text;
            this.form.RandomData = textBox2.Text;
            this.Close();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
