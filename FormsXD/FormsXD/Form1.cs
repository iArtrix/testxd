﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsXD
{
    public partial class Form1 : Form
    {
        public String SendData { get; set; }
        public String RandomData { get; set; }
        public int PositionStos { get; set; }
        public int PositionTable { get; set; }
        private Stos st;
        private Tablica tablica;
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            treeView1.ExpandAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var form = new Form2(this);
            form.ShowDialog();
            if(this.SendData != null)
            {
                int v;
                if(int.TryParse(this.SendData, out v))
                {
                    this.textBox1.AppendText(this.SendData + "\n");
                    st = new Stos(v);
                    treeView1.Nodes.Add("Stos");
                    int i = 0;
                    foreach (double val in st.Show())
                    {
                        treeView1.Nodes[PositionStos].Nodes.Add("["+ i.ToString() + "] " + val.ToString());
                        i++;
                    }
                    this.textBox1.AppendText(PositionStos.ToString() + "\n");
                    treeView1.ExpandAll();
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var form = new Form2(this);
            form.ShowDialog();
            if (this.SendData != null)
            {
                int v;
                if (int.TryParse(this.SendData, out v))
                {
                    this.textBox1.AppendText(this.SendData + "\n");
                    st.Push(v);
                    treeView1.Nodes[PositionStos].Nodes[st.pos-1].Text = ("[" + (st.pos-1) + "] " + SendData);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            st.Pop();
            treeView1.Nodes[PositionStos].Nodes[st.pos].Text = ("[" + (st.pos) + "] 0");
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            var form = new Form3(this);
            form.ShowDialog();
            if (this.SendData != null)
            {
                int v;
                int rv;
                if (int.TryParse(this.SendData, out v))
                {
                    int.TryParse(this.RandomData, out rv);
                    this.textBox1.AppendText(this.SendData + "\n");
                    if (this.RandomData != null)
                        tablica = new Tablica(v, rv);
                    else
                        tablica = new Tablica(v);
                    treeView1.Nodes.Add("Tablica");
                    int i = 0;
                    foreach (double val in tablica.Show())
                    {
                        treeView1.Nodes[0].Nodes.Add("[" + i.ToString() + "] " + val.ToString());
                        i++;
                    }
                    this.textBox1.AppendText(PositionTable.ToString() + "\n");
                    treeView1.ExpandAll();
                }
            }
        }
    }
}
